import time
import threading
import multiprocessing
# from MongoQueue import MongoQueue
from Download import request
from bs4 import BeautifulSoup
import hashlib
import base64
import re
import requests
from pymongo import MongoClient, errors

SLEEP_TIME = 1

def make_hash_sha256(o):
    hasher = hashlib.sha256()
    hasher.update(repr(make_object_hashable(o)).encode())
    return base64.b64encode(hasher.digest()).decode()

def make_object_hashable(o):
    # make extensible for any collection in python
    if isinstance(o, (tuple, list)):
        return tuple((make_object_hashable(e) for e in o))

    if isinstance(o, dict):
        return tuple(sorted((k, make_object_hashable(v)) for k,v in o.items()))

    if isinstance(o, (set, frozenset)):
        return tuple(sorted(make_object_hashable(e) for e in o))

    return o

def extract_phone_number(text_body):
    phonePattern = re.compile(r'''
    # don't match beginning of string, number can start anywhere
    (\d{3})     # area code is 3 digits (e.g. '800')
    \D*         # optional separator is any number of non-digits
    (\d{3})     # trunk is 3 digits (e.g. '555')
    \D*         # optional separator
    (\d{4})     # rest of number is 4 digits (e.g. '1212')
    \D*         # optional separator
    (\d*)       # extension is optional and can be any number of digits
    $           # end of string
    ''', re.VERBOSE)

    if text_body is not None:
        try:
            phone_number_groups = phonePattern.search(str(text_body)).groups()
            return  ''.join(phone_number_groups)
        except:
            raise
    else:
        return ""

def search(text,n):
    '''Searches for text, and retrieves n words either side of the text, which are returned seperatly'''
    word = r"\W*([\w]+)"
    groups = re.search(r'{}\W*{}{}'.format(word*n,'place',word*n), text).groups()
    return groups[:n],groups[n:]

def parse(posting_url):
    html = None    
    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 6.1; WOW64)"
                      " AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"
    }
    try:
        r = requests.get(posting_url, headers=headers)
        time.sleep(5)
        if r.status_code == 200:
            posting_data = dict()
            html = r.text
            posting_data['absoluteUrl'] = r.url
            soup = BeautifulSoup(html, 'lxml')
            posting_data['postingTitle'] = soup.find(id = "postingTitle").h1.string
            posting_data['postingDate'] =  soup.find("div", {"class": "adInfo"}).text.replace('Posted:', '').strip()
            rawText = soup.find("div", {"class": "postingBody"}).text
            if rawText is not None:
                posting_data['rawText'] = rawText
                # tuple of numbers that are then joined         
                posting_data['cellPhoneNumber'] = extract_phone_number(rawText)
            else:
                posting_data['rawText'] = "No text body in this post"

            # parent of all other children in advertisement content        
            posting = soup.find("div", {"class": "posting"})
            if posting is not None:
                posting_data['socialProfile']  = posting.find("a")['href']
                # metadata
                metaData = posting.find("p", {"class" : "metaInfoDisplay"})        
                serviceProviderAge = metaData.text.split(' ')[-1]
                posting_data['serviceProviderAge'] = serviceProviderAge
                # convert all text into single string
                focus = list(map(lambda x: x.text, posting.findAll('div')))[0].replace('\n', '')        
                # just words
                words = re.findall(r'\w+', focus)
                for index, word in enumerate(words):
                    if word == 'Post':
                        posting_id = words[index + 2]
                        posting_data['postingID'] = posting_id                        
                    if word == 'Location':
                        service_location =  words[index + 2]
                        posting_data['serviceLocation'] = service_location
    except Exception as ex:
        print(str(ex))
    finally:
        if len(posting_data.items()) > 0:
            return posting_data
        else:
            return None   

def getDocuments():
    client = MongoClient()
    client_db = client['backpage_nc']
    collection = client_db.crawl_queue
    for post in collection.find():
        url = post['_id']
        post_mapping = parse(url)
        try:
            collection.update_one({'_id':url}, 
                {"$set": post_mapping}, 
                upsert=False
            )
        except errors.DuplicateKeyError as e:
            print(e)
            pass

# def pageurl_crawler():
#     crawl_queue = MongoQueue('backpage_nc', 'crawl_queue')
#     post_queue = MongoQueue('backpage_nc', 'post_queue') 
#     while True:
#         try:
#             url = crawl_queue.pop()
#             print(url)
#         except KeyError:
#             print('Key does not exist')
#         else:
#             url = crawl_queue.pop_url(url)
#             post_mapping = parse(url)
#             if post_mapping != None:
#                 print("Post mapping " + str(post_mapping))
#                 post_queue.push_post(url, post_mapping)
#                 crawl_queue.complete(url)
#                 print('Finished with post - onto the next!')
#             else:
#                 crawl_queue.push(url)
#                 crawl_queue.repair()         

#     threads = []
#     while crawl_queue:
#         """
#         Here crawl_queue used up, that is, we __bool__ function of the role of the true representative of our MongoDB queue there are data
#         threads or crawl_queue are true on behalf if we have not yet completed the download, the program will continue
#         """
#         for thread in threads:
#             if not thread.is_alive():  # to determine whether it is empty, not empty is deleted in the queue
#                 threads.remove(thread)
#         # thread in the thread pool is less than max_threads or crawl_queue
#         while len(threads) < max_threads or crawl_queue.peek():
#             thread = threading.Thread(target=pageurl_crawler) 
#             thread.setDaemon(True)  # Set the daemon thread
#             thread.start()
#             threads.append(thread)
#         time.sleep(SLEEP_TIME)

# def process_crawler():
#     process = []
#     num_cpus = multiprocessing.cpu_count()
#     print('Number of CPUs available', num_cpus)
#     for i in range(num_cpus):
#         p = multiprocessing.Process(target=backpage_crawler)
#         p.start()
#         process.append(p)
#     for p in process:
#         p.join()  # Wait for the process inside the process queue to end


# def process_crawl():
#     cpu_nums = multiprocessing.cpu_count()
#     p = multiprocessing.Pool(cpu_nums)

#     print(u'Number of CPUs:', cpu_nums)

#     for i in range(cpu_nums + 1):
#         p.apply_async(pageurl_crawler)
#     p.close()
#     p.join()

if __name__ == '__main__':
    getDocuments()
    # start(spider_queue)
    # process_crawl()    
