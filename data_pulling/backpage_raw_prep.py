import json
import csv
import pandas as pd
from data_pulling.tna_data_prep import DataClean

# Read raw data
with open('data/backpage_new.json', 'r') as f:
     data = json.load(f)

# Raw data is a list of lists. Convert it to pandas dataframe.
data = pd.DataFrame(data)

# First row is column names. Use it to rename the columns and then remove it.
data.columns = data.iloc[0]
data = data.iloc[1:, :]
data.reset_index(drop=True, inplace=True)

# Except for the img_urls column, each row in each column is a list of one element.
# We unwrap the lists for all but img_urls.
for i in xrange(8):
    data.iloc[:, i] = data.iloc[:, i].apply(pd.Series)

# The emoji's all have double forward slashes, need to replace them with single
# slashes before decoding. Let's just say figuring out how to do this wasn't easy.
data['title'] = \
    data['title'].str.encode('utf-8').str.decode('string-escape').str.decode('utf-8')
data['body'] = \
    data['body'].str.encode('utf-8').str.decode('string-escape').str.decode('utf-8')

# Clean data. DataClean() assumes that the column for the body of ad is called raw.
data.rename(columns={'body': 'raw'}, inplace=True)
data = DataClean(data, strip_tab=True, strip_line_break=True, strip_punctuation=True,
                 convert_lower_case=True, find_number=True, remove_number=True,
                 unicode_text=True)

data.to_pickle('data/backpage_cleaned.pkl')


# Obtained another dataset from Yeng in Dec. 2017. We clean it like before (above).
with open('data/backpageprev_body.json', 'r') as f:
    data2 = json.load(f)

data2 = pd.DataFrame(data2)
data2.columns = ['postid', 'location', 'city', 'raw']

# From a preliminary inspection, certains rows are filled with garbage and should be
# dropped
data2 = data2[~data2['raw'].str.contains('nav search')]

# Remove double forward slashes. Code is different from before because we were
# dealing with \\x bullshit and now it's \\u bullshit. End goal is still to make
# emojis in the same format as the ones in emoji.UNICODE_EMOJI.keys().
# Also need to replace some special cases to avoid a truncated \uXXXX escape error.
data2['raw'] = data2['raw'].str.replace('\\un', ' un').str.replace(
    '\\use', ' use').str.replace('\\up', ' up')
for i in xrange(data2.shape[0]):
    try:
        data2['raw'].iloc[i] = data2['raw'].iloc[i].decode('unicode_escape').encode(
            'utf-8').decode('utf-8')
    except UnicodeDecodeError:
        # More trimming
        data2['raw'].iloc[i] = data2['raw'].iloc[i].replace('\\u\\', 'u').replace(
            '\\x', 'x').replace('\\uc', 'uc').replace('\\ur', 'ur')
        data2['raw'].iloc[i] = data2['raw'].iloc[i].decode('unicode_escape').encode(
            'utf-8').decode('utf-8')

# Make a placeholder column as DataClean() expects a title columns.
data2['title'] = ''

data2 = DataClean(data2, strip_tab=True, strip_line_break=True,
                  strip_punctuation=True, convert_lower_case=True, find_number=False,
                  remove_number=True, unicode_text=True)

# Drop empty rows
data2 = data2[data2.raw != '']

data2.to_pickle('data/backpage_cleaned2.pkl')

# Check to see if the overlap with older datasets is negligible.
backpage1 = pd.read_pickle('data/backpage_cleaned.pkl')
tna = pd.read_pickle("data/tna_ad.pkl")
traffic10k = pd.read_pickle('data/trafficking10k_cleaned.pkl')
print pd.Series(list(set(data2.raw) & set(backpage1.raw)))
print pd.Series(list(set(data2.raw) & set(tna.raw)))
print pd.Series(list(set(data2.raw) & set(traffic10k.raw.str.decode('utf-8'))))

# Dump to txt for word2vec training
data2.raw.to_frame().to_csv(
    r'data/escort_texts.txt', header=None, index=None, sep=' ', mode='a',
    encoding='utf-8', quoting=csv.QUOTE_NONE, escapechar=' ')