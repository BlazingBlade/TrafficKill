import docx
import pandas as pd
from tna_data_prep import DataClean

doc = docx.Document('data/verified_cases_cleaned.docx')

# Each doc.paragraphs[i] contains a single line in the original WORD doc. Each ad can
# have multiple lines. Two ads are separated by one or more empty lines. We wish to
# create a list where each entry is a complete ad.
ad_list = []
ad = u''
for i in xrange(len(doc.paragraphs)):
    # Concatenate the lines of a single ad
    if doc.paragraphs[i].text != u'':
        ad = ad + u' ' + doc.paragraphs[i].text
    # The first empty line after a non-empty line marks the end of an ad, where we
    # move on to the next ad.
    elif doc.paragraphs[i - 1].text != u'':
        ad_list.append(ad)
        ad = u''

# Convert to pandas dataframe. Here the title and body of an ad are already merged,
# but we still create an empty title column because DataClean()expects both title and
# body
data = pd.DataFrame({'title': '', 'raw': ad_list})

# Clean data
data = DataClean(data, strip_tab=True, strip_line_break=True, strip_punctuation=True,
                 convert_lower_case=True, find_number=False, remove_number=True)
data.to_pickle("data/verified_cases_cleaned.pkl")


# Test section
for i in xrange(len(ad_list)):
    print data['raw'].iloc[i].encode('utf-8')
    print ad_list[i].encode('utf-8')