import emoji
import string
import re
import time
import numpy as np
import pandas as pd
from elasticsearch import Elasticsearch


def DataPull(post_type="ad", variables='All', max_result=500000):
    """
    Pull data from tnaboard.com and store them in pandas dataframe
    :param post_type: Post type. Choices are ["ad", "review", "thread"].
    :param variables: Variables of interest. Set to 'All' to select all variables,
        otherwise specify a list of variable names.
    :param max_result: Maximum number of results allowed.
    :return: A pandas dataframe containing variables of interest.
    """
    # connect to elasticsearch
    es = Elasticsearch(['recommender.oscar.ncsu.edu'],
                       http_auth=('elastic', 'changeme'),
                       port=9200)
    es.indices.put_settings(index="gen",
                            body={"index": {"max_result_window": max_result}})

    data = es.search(index="gen", doc_type="post",
                     body={"query": {"match": {"post_type": post_type}}})
    # By default only 10 entries are returned. Find the total size and pull again.
    size = np.minimum(data['hits']['total'], max_result)
    data = es.search(index="gen", doc_type="post",
                     body={"query": {"match": {"post_type": post_type}},
                           "size": size})

    # Store variables of interest in a pandas dataframe
    # From the previous step, data['hits']['hits'] is a list of dictionaries, which
    # can be directly converted to pandas.
    data = pd.DataFrame(data['hits']['hits'])
    # Now, data['_source'] is itself a dictionary. We further split it.
    data = data['_source'].apply(pd.Series)
    # Only keep pre-specified variables
    if variables != 'All':
        data = data[variables]

    return data


def StripPunctuation(input_string):
    if input_string is None:
        return ''

    regex = re.compile('[%s]' % re.escape(string.punctuation))

    return regex.sub(' ', input_string)


def StripExtraSpace(input_string):
    if input_string is None:
        return ''

    return re.sub('\s{2,}', ' ', input_string.strip())


def FindNumber(input_string):
    """
    Matches 0123456789, 012.345.6789, 012-345-6789, 012 345 6789, (012) 345 6789 and
    all combinations thereof.
    :return: list of strings of phone numbers
    """
    if input_string is None:
        return []

    number_list = re.findall(
        r"\(?\b[2-9][0-9]{2}\)?[-. ]?[2-9][0-9]{2}[-. ]?[0-9]{4}\b", input_string)
    # Remove possible duplicates
    number_list = np.unique(number_list).tolist()

    return number_list


def RemoveNumber(input_string):
    """
    Matches 0123456789, 012.345.6789, 012-345-6789, 012 345 6789, (012) 345 6789 and
    all combinations thereof.
    :return: String with phone numbers of the above format removed
    """
    if input_string is None:
        return ''

    output_string = re.sub(
        r'\(?\b[2-9][0-9]{2}\)?[-. ]?[2-9][0-9]{2}[-. ]?[0-9]{4}\b', ' ',
        input_string)

    return output_string


def DataClean(data, strip_tab=True, strip_line_break=True, strip_punctuation=True,
              convert_lower_case=True, find_number=True, remove_number=True,
              unicode_text=True):
    """
    Clean the texts in the pulled raw data, assuming that the data contain columns
    'raw' and 'title'.
    :param data: dataframe produced by data_pull()
    :param strip_tab: Whether tab (\t) should be stripped
    :param strip_line_break: Whether line break (\n or <br>) should be stripped
    :param strip_punctuation: Whether all punctuations should be stripped
    :param covert_lower_case: Whether all texts should be converted to lower case
    :param find_number: Whether phone numbers should be extracted and stored in the
    'phone' column
    :param remove_number: Whether phone numbers from the original texts should be
    removed (so they don't become words in the dictionary)
    :param unicode_text: Whether the text input is in unicode (or 'utf-8')
    :return: dataframe with texts cleaned
    """
    # data['raw'] is a dictionary with one key, 'body_text'. We get rid of the
    # dictionary layer.
    data['raw'] = data['raw'].apply(pd.Series)

    # Replace nan with empty strings
    data['title'] = data['title'].fillna('')
    data['raw'] = data['raw'].fillna('')

    # Add space around emojis so they can be tokenized properly
    # Unicode turns out to be hard to handle in Python2. Will use str.replace()
    # for every emoji separately. Wonder whether there's a more efficient way.
    emoji_list = emoji.UNICODE_EMOJI.keys()
    # Add bullet point to the list
    emoji_list.append(u'\u25cf')
    padded_emoji_list = [' ' + e + ' ' for e in emoji_list]
    for i in xrange(len(emoji_list)):
        # Somehow emoji_list[179] will cause an error. Use try-except to bypass.
        try:
            if unicode_text:
                data['raw'] = data['raw'].str.replace(emoji_list[i],
                                                      padded_emoji_list[i])
                data['title'] = data['title'].str.replace(emoji_list[i],
                                                          padded_emoji_list[i])
            else:
                data['raw'] = data['raw'].str.replace(
                    emoji_list[i].encode('utf-8'),
                    padded_emoji_list[i].encode('utf-8'))
                data['title'] = data['title'].str.replace(
                    emoji_list[i].encode('utf-8'),
                    padded_emoji_list[i].encode('utf-8'))
        except Exception:
            pass

    # Strip tabs or line breaks or punctuations if requested. Replace them with
    # spaces for tokenization purposes. It's ok to have extra spaces because we'll
    # strip them later, but we don't want two words unseparated.
    if strip_tab:
        data['raw'] = data['raw'].str.replace('\t', ' ')
        data['title'] = data['title'].str.replace('\t', ' ')
    if strip_line_break:
        data['raw'] = data['raw'].str.replace('\n', ' ')
        data['raw'] = data['raw'].str.replace('<br>', ' ')
        data['title'] = data['title'].str.replace('\n', ' ')
        data['title'] = data['title'].str.replace('<br>', ' ')
    if strip_punctuation:
        # Consider extracting emails before stripping punctuation.
        data['raw'] = data['raw'].apply(StripPunctuation)
        data['title'] = data['title'].apply(StripPunctuation)

    # Strip extra space
    data['raw'] = data['raw'].apply(StripExtraSpace)
    data['title'] = data['title'].apply(StripExtraSpace)

    # Extract phone numbers
    if find_number:
        phone_from_raw = data['raw'].apply(FindNumber)
        phone_from_title = data['title'].apply(FindNumber)
        # Delete duplicates because the same number can be in both raw and title
        for i in xrange(data.shape[0]):
            phone_from_raw[i] = np.unique(phone_from_raw[i] +
                                          phone_from_title[i]).tolist()
        data['phone'] = phone_from_raw

    # Remove phone numbers from original texts
    if remove_number:
        data['raw'] = data['raw'].apply(RemoveNumber)
        data['title'] = data['title'].apply(RemoveNumber)

    # Convert to lowercase
    if convert_lower_case:
        data['raw'] = data['raw'].str.lower()
        data['title'] = data['title'].str.lower()

    return data


if __name__ == "__main__":
    # Pull each post_type separately if encountering memory error.
    for post_type in ["ad", "review", "thread"]:
        start_time = time.time()
        data = DataPull(post_type=post_type)
        print "Minutes to pull", post_type, ":", (time.time() - start_time) / 60
        start_time = time.time()
        data = DataClean(data)
        print "Minutes to clean", post_type, ":", (time.time() - start_time) / 60
        data.to_pickle("tna_" + post_type + ".pkl")
