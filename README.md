# Data Pulling and Scraping workflow

## To Get Up and Running (with the assumption that interpreter version is Python 3+)

## Create Virtualenv and Activate the Virtualenv

* `$ virtualenv {whatever you would like to call your environment}`
* `$ source {your environment}/bin/activate`

## Install Dependencies

* `$ pip install -U -r requirements.txt`

## Deactivating Virtualenv at any time

* `$ deactivate`

## Assuming you do the following first:

* `$ cd traffic`

# Python scripts to note

### spider.py

    python spider.py

This script does the following:

+ Makes requests to every posting on North Carolina's Backpage site for "WomenSeekMen" category
+ Scrapes all post URI's and indexes them into Mongo Collection `crawl_queue`

### postings_crawler.py

    python postings_crawler.py

This script does the following:

+ For each individual post, scrapes data and updates index in `crawl_queue` collection
+ TODO: Initiate a scheduler to run in the background on a separate thread
+ During a designated, UDF interval, execute another script, ```executor.py```. 

## Overview of Data Model

 The post object represent the following model:

 ```
Post = {
  absoluteUrl: { type: String' },
  rawText: { type: String },
  status: { type: Number },
  serviceProviderAge: { type: Number },  
  timestamp: { type: DateTime },
  postingTitle: { type: String },
  socialProfile: { type: String },
  postingDate: { type: String },
  cellPhoneNumber: { type: String }, # a tag for comment referenced in reply,
  _id: {type : String} # unique URI

```

## Built With

* [Requests](https://github.com/requests/requests) - Open-Source module for sending HTTP/1.1 requests
* [bs4](https://pypi.python.org/pypi/beautifulsoup4) - Open-Source Framework for Extracting Data for Scraping Data from Websites
* [PyMongo](https://api.mongodb.com/python/current/) - Python Distribution for MongoDB

 
## MongoDB References
    
  - [MongoDB Support](https://docs.mongodb.org/manual/support/)

