"""
Obtains prediction word by word for a single sentence, i.e., for all sub-
sequences of the sentence that start from the beginning. Useful for examining
how prediction changes as sentence goes on.
"""
from models.GFRNN import *
pd.options.display.max_rows = 200

# Multi-class model
# Load data
data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
data['raw'] = data['title'] + ' ' + data['raw']
train_dat, valid_dat, test_dat = utility.DataSplit(data, 0.7, 0.15, seed=0,
                                                   verbose=False)

# Train model
rnn_classifier = RNNClassifier(output_layer_type='mean-pool')
rnn_classifier.build_vocabulary(train_dat['raw'])
rnn_classifier.initialize_model()
rnn_classifier.train_model(
    x_train=train_dat['raw'], y_train=train_dat['label'].values,
    x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

# Evaluate model
_, val_pred = rnn_classifier.predict(valid_dat['raw'], use_gpu=True)
val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
val_cs = utility.CumulativeScore(val_pred, valid_dat['label'].values,
                                 tolerance=0)

_, test_pred = rnn_classifier.predict(test_dat['raw'], use_gpu=True)
test_mae = utility.MeanAbsError(test_pred, test_dat['label'].values)
test_cs = utility.CumulativeScore(test_pred, test_dat['label'].values,
                                  tolerance=0)

print 'Validation mean abs error: %5.3f | Validation cumulative score: %0.3f | ' \
      'Test mean abs error: %5.3f | Test cumulative score: %0.3f' % (
    val_mae, val_cs, test_mae, test_cs)
print confusion_matrix(test_dat['label'].values, test_pred)

# Check out the binary classification performance of the ordinal regression model
pred_label = utility.GroupLabels(test_pred, cutoff=3, verbose=False)
truth = utility.GroupLabels(test_dat['label'], cutoff=3, verbose=False)
test_acc = np.mean(pred_label == truth)
test_weighted_acc = utility.WeightedACC(pred_label, truth)
print 'Binary classification test_acc: %5.3f | test_weighted_acc: %5.3f' % (
    test_acc, test_weighted_acc
)
print confusion_matrix(truth, pred_label)

# Extract texts for true positive and true negative predictions, i.e., the ones for
# which our trained model did well.
true_pos_text = test_dat['raw'].loc[(test_dat['label'] >= 4) & (test_pred >= 4)]
true_neg_text = test_dat['raw'].loc[(test_dat['label'] < 4) & (test_pred < 4)]

# Functions that show the word by word predictions
def show_pos(i):
    word_list, pred_prob, pred_label = rnn_classifier.word_by_word_predict(
    true_pos_text.iloc[i], use_gpu=True)

    return pd.DataFrame(
        {'words': word_list,
         'probs': np.sum(pred_prob[:, 4:], axis=1)})[['words', 'probs']]

def show_neg(i):
    word_list, pred_prob, pred_label = rnn_classifier.word_by_word_predict(
    true_neg_text.iloc[i], use_gpu=True)

    return pd.DataFrame(
        {'words': word_list,
         'probs': np.sum(pred_prob[:, 4:], axis=1)})[['words', 'probs']]

pos = []
for i in xrange(50):
    pos.append(show_pos(i))

with open('word_by_word_samples.pkl', 'wb') as f:
    pickle.dump(pos, f)
