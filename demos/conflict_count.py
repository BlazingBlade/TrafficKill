"""
Count the instances where monotonicity of produced binary predictions are violated.
E.g., if the predicted P(class > 2) < P(class > 3), it's a violation.
"""

from __future__ import division
import numpy as np
import pandas as pd
import numpy.random as nr
from sklearn.model_selection import KFold
from models.GFRNN_ord_reg import OrdinalRNNClassifier

# Load data
data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
data['raw'] = data['title'] + ' ' + data['raw']
train_size = int(data['raw'].shape[0] * 0.7)
n_class = 7

nr.seed(0)
data = data.sample(frac=1, random_state=0)
CV_index = KFold(n_splits=10, shuffle=True)

count_with_penalty = np.zeros(10)
count_no_penalty = np.zeros(10)
percent_with_penalty = np.zeros(10)
percent_no_penalty = np.zeros(10)

i = 0
for train_index, test_index in CV_index.split(data):
    train_dat = data.iloc[train_index, :][:train_size]
    valid_dat = data.iloc[train_index, :][train_size:]
    test_dat = data.iloc[test_index, :]

    # Model with order penalty
    model = OrdinalRNNClassifier(
        order_penalty=1,
        pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
        pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
    model.build_vocabulary(train_dat['raw'])
    model.initialize_model()
    model.train_model(
        x_train=train_dat['raw'], y_train=train_dat['label'].values,
        x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

    pred_prob, _ = model.predict(test_dat['raw'], use_gpu=True)
    # pred_prob is of shape (n_sample, n_class). Count the number of times
    # monotonicity is violated.
    count_with_penalty[i] = np.sum((pred_prob[:, :n_class-2] - pred_prob[:, 1:]) < 0)

    # Repeat for model with no order penalty
    model = OrdinalRNNClassifier(
        order_penalty=0,
        pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
        pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
    model.build_vocabulary(train_dat['raw'])
    model.initialize_model()
    model.train_model(
        x_train=train_dat['raw'], y_train=train_dat['label'].values,
        x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

    pred_prob, _ = model.predict(test_dat['raw'], use_gpu=True)
    count_no_penalty[i] = np.sum((pred_prob[:, :n_class-2] - pred_prob[:, 1:]) < 0)

    n_test_sample = test_dat.shape[0]
    total_binary_predictions = test_dat.shape[0] * n_class
    percent_with_penalty[i] = count_with_penalty[i] / total_binary_predictions
    percent_no_penalty[i] = count_no_penalty[i] / total_binary_predictions

    print 'rep: %2d | n_test_sample: %5d | total_binary_predictions: %5d | ' \
          'count_with_penalty: %5d | count_no_penalty: %5d | ' \
          'percent_with_penalty: %0.3f | percent_no_penalty: %0.3f' % (
        i, n_test_sample, total_binary_predictions, count_with_penalty[i],
        count_no_penalty[i], percent_with_penalty[i], percent_no_penalty[i])

    i += 1

# Print summaries
np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
print '\ncount_with_penalty:', count_with_penalty, \
    '\nmean: %5.3f' % count_with_penalty.mean(), \
    'stderr: %5.3f' % (count_with_penalty.std() / 10**0.5), \
    '\ncount_no_penalty:', count_no_penalty, \
    '\nmean: %5.3f' % count_no_penalty.mean(), \
    'stderr: %5.3f' % (count_no_penalty.std() / 10**0.5), \
    '\npercent_with_penalty:', percent_with_penalty, \
    '\nmean: %5.3f' % percent_with_penalty.mean(), \
    'stderr: %5.3f' % (percent_with_penalty.std() / 10**0.5), \
    '\npercent_no_penalty:', percent_no_penalty, \
    '\nmean: %5.3f' % percent_no_penalty.mean(), \
    'stderr: %5.3f' % (percent_no_penalty.std() / 10**0.5), '\n'