import time
import numpy as np
import pandas as pd
import models.utility as utility
from models.GFRNN import RNNClassifier
from models.GFRNN_ord_reg import OrdinalRNNClassifier
from models.GFRNN_cont_reg import RNNRegression
from models.baseline_classification import BaselineClassifier


# Load trafficking10k as training and validation data
data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
data['raw'] = data['title'] + ' ' + data['raw']
train_dat, valid_dat, _ = utility.DataSplit(data, train_frac=0.8, valid_frac=0.2,
                                            seed=0, verbose=False)

# Load the verified cases as test data and assign label 6 (extremely likely) to the
# verified cases. Also need to make sure that the verified cases have the same
# encoding as the training data (utf-8).
test_dat = pd.read_pickle('data/verified_cases_cleaned.pkl')
test_dat['label'] = 6
test_dat['raw'] = test_dat['raw'].str.encode('utf-8')
# test_dat['raw'] = test_dat['raw'].str.replace(' phone number ', ' ')

# Redefine all metrics function, excluding macro_mae and weighted_acc
def AllMetrics(prediction, label, num_target_classes=7, binary_cutoff=3):
    mae = utility.MeanAbsError(prediction, label)
    cs0 = utility.CumulativeScore(prediction, label, tolerance=0)
    cs1 = utility.CumulativeScore(prediction, label, tolerance=1)
    bin_acc = np.mean(utility.GroupLabels(prediction, binary_cutoff, False) ==
                      utility.GroupLabels(label, binary_cutoff, False))

    return mae, cs0, cs1, bin_acc


# gfrnn_classification:
start_time = time.time()
model = RNNClassifier(
    pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
    pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
model.build_vocabulary(train_dat['raw'])
model.initialize_model()
model.train_model(
    x_train=train_dat['raw'], y_train=train_dat['label'].values,
    x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

_, test_pred = model.predict(test_dat['raw'], use_gpu=True)
mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 2.650 | cs0: 0.000 | cs1: 0.003 | bin_acc: 0.672 | min: 10.30
# (array([2, 3, 4, 5]), array([102,   1, 210,   1]))


# gfrnn_ord_reg:
start_time = time.time()
model = OrdinalRNNClassifier(
    order_penalty=1,
    pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
    pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
model.build_vocabulary(train_dat['raw'])
model.initialize_model()
model.train_model(
    x_train=train_dat['raw'], y_train=train_dat['label'].values,
    x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

_, test_pred = model.predict(test_dat['raw'], use_gpu=True)
mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 2.783 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.532 | min: 14.26
# (array([1, 2, 3, 4]), array([  1,  97,  49, 167]))


# gfrnn_ord_reg_no_penalty:
start_time = time.time()
model = OrdinalRNNClassifier(
    order_penalty=0,
    pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
    pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
model.build_vocabulary(train_dat['raw'])
model.initialize_model()
model.train_model(
    x_train=train_dat['raw'], y_train=train_dat['label'].values,
    x_val=valid_dat['raw'], y_val=valid_dat['label'].values)

_, test_pred = model.predict(test_dat['raw'], use_gpu=True)
mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 2.812 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.519 | min: 14.18
# (array([1, 2, 3, 4]), array([  1, 102,  48, 163]))


# gfrnn_regression:
start_time = time.time()
model = RNNRegression(
    pretrain_vocab_file_path='trained_models/escort_vocab_mini5_128len_v2.txt',
    pretrain_embedding_file_path='trained_models/escort_embedding_mini5_128len_v2')
model.build_vocabulary(train_dat['raw'])
model.initialize_model()
model.train_model(x_train=train_dat['raw'],
                  y_train=train_dat['label'].values.reshape(-1, 1),
                  x_val=valid_dat['raw'],
                  y_val=valid_dat['label'].values.reshape(-1, 1))

test_pred = model.predict(test_dat['raw'], use_gpu=True)
mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 2.889 | cs0: 0.000 | cs1: 0.006 | bin_acc: 0.223 | min: 11.46
# (array([1., 2., 3., 4., 5.], dtype=float32), array([  1,  35, 208,  68,   2]))


# logistic_immediate_thresh:
start_time = time.time()
best_val_mae = 1.0
best_alpha = None
for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
    model = BaselineClassifier(
        vectorizer_name='tf-idf',
        dimension_reduction_name=None,
        classification_name='ord_logistic_immediate_threshold', seed=0,
        n_gram_low=1, n_gram_high=1,
        min_frequency=2,
        sublinear_tf=True, alpha=alpha)
    model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

    val_pred = model.predict_pipeline(valid_dat['raw'])
    val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
    if val_mae < best_val_mae:
        best_val_mae = val_mae
        best_alpha = alpha
        test_pred = model.predict_pipeline(test_dat['raw'])
        mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 2.825 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.589 | min:  0.39
# (array([1, 2, 4]), array([  1, 128, 185]))


# logistic_all_thresh:
start_time = time.time()
best_val_mae = 1.0
best_alpha = None
for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
    model = BaselineClassifier(
        vectorizer_name='tf-idf',
        dimension_reduction_name=None,
        classification_name='ord_logistic_all_threshold', seed=0,
        n_gram_low=1, n_gram_high=1,
        min_frequency=2,
        sublinear_tf=True, alpha=alpha)
    model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

    val_pred = model.predict_pipeline(valid_dat['raw'])
    val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
    if val_mae < best_val_mae:
        best_val_mae = val_mae
        best_alpha = alpha
        test_pred = model.predict_pipeline(test_dat['raw'])
        mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 3.401 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.178 | min:  0.51
# (array([1, 2, 3, 4]), array([  1, 180,  77,  56]))


# least_abs_deviation:
start_time = time.time()
best_val_mae = 1.0
best_alpha = None
for alpha in [0.1, 0.5, 1.0, 5.0, 10.0]:
    model = BaselineClassifier(
        vectorizer_name='tf-idf',
        dimension_reduction_name=None,
        classification_name='least_abs_deviation', seed=0,
        n_gram_low=1, n_gram_high=1,
        min_frequency=2,
        sublinear_tf=True, alpha=alpha)
    model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

    val_pred = model.predict_pipeline(valid_dat['raw'])
    val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
    if val_mae < best_val_mae:
        best_val_mae = val_mae
        best_alpha = alpha
        test_pred = model.predict_pipeline(test_dat['raw'])
        mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 3.318 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.076 | min:  0.10
# (array([1., 2., 3., 4.]), array([  3, 118, 169,  24]))


# logistic_regression:
start_time = time.time()
model = BaselineClassifier(
    vectorizer_name='tf-idf',
    dimension_reduction_name=None,
    classification_name='logistic_regression', seed=0,
    n_gram_low=1, n_gram_high=1,
    min_frequency=2,
    sublinear_tf=True)
model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

test_pred = model.predict_pipeline(test_dat['raw'])
mae, cs0, cs1, bin_acc = AllMetrics(test_pred, test_dat['label'].values)
print 'mae: %0.3f | cs0: %0.3f | cs1: %0.3f | bin_acc: %0.3f | min: %5.2f' % (
    mae, cs0, cs1, bin_acc, (time.time() - start_time) / 60)
print np.unique(test_pred, return_counts=True)
# mae: 3.188 | cs0: 0.000 | cs1: 0.000 | bin_acc: 0.424 | min:  0.02
# (array([0, 1, 2, 4]), array([  2,   7, 172, 133]))