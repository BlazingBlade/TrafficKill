import csv
import os
import copy
import torch
import numpy as np
import numpy.random as nr
import pandas as pd
import torch.nn as nn
from torch.autograd import Variable


def DataSplit(data, train_frac=0.7, valid_frac=0.15, seed=123, verbose=True):
    """
    Split a dataset into train, val, test data
    """
    n_sample = data.shape[0]
    # Shuffle data, then split into 3 folds
    train, valid, test = np.split(
        data.sample(frac=1, random_state=seed),
        [int(train_frac * n_sample), int((train_frac + valid_frac) * n_sample)])
    # Print data distribution
    if verbose:
        print "Train distribution:\n", train.label.value_counts()
        print "Val distribution:\n", valid.label.value_counts()
        print "Test distribution:\n", test.label.value_counts()

    return train, valid, test


def WeightedACC(prediction, truth):
    """
    Compute weighted accuracy as described on page 6 of
    https://arxiv.org/pdf/1705.02735.pdf
    """
    neg = np.sum(truth == 0)
    pos = np.sum(truth == 1)
    true_neg = np.sum((prediction == 0) & (truth == 0))
    true_pos = np.sum((prediction == 1) & (truth == 1))
    weighted_acc = 0.5 * (true_pos / float(pos) + true_neg / float(neg))

    return weighted_acc


def GroupLabels(label, cutoff=3, verbose=True):
    """
    Separate labels into two groups
    :param label: Original labels, which are finer defined
    :param cutoff: Cutoff value for the separation
    :return: New labels
    """
    new_label = copy.deepcopy(label)
    new_label[label <= cutoff] = 0
    new_label[label > cutoff] = 1
    if verbose:
        print "Pre-grouping distribution:\n", label.value_counts()
        print "Post-grouping distribution:\n", new_label.value_counts()

    return new_label


def OrdinalLabelReformat(label, num_target_classes):
    new_label = np.zeros(shape=(len(label), num_target_classes - 1), dtype=int)
    for i in xrange(len(label)):
        new_label[i, :label[i]] = 1

    return new_label


def MeanAbsError(prediction, label):
    """
    Refer to Section 5.2 in https://goo.gl/T5jX6z.
    """

    return np.mean(np.abs(prediction - label))


def CumulativeScore(prediction, label, tolerance=0):
    """
    Refer to Section 5.2 in https://goo.gl/T5jX6z.
    Except that I decided to not take the negative which doesn't make sense.
    """

    return np.mean(np.abs(prediction - label) <= tolerance)


def TaskWeight(label, num_target_classes, scheme='uniform'):
    """
    Refer to Section 5.4.1 in https://goo.gl/T5jX6z.
    """
    if scheme == 'uniform':

        return np.ones(num_target_classes - 1)

    elif scheme == 'data-specific':
        # Assuming there's no empty class.
        _, counts = np.unique(label, return_counts=True)
        weights = np.sqrt(counts) / np.sum(np.sqrt(counts))

        return weights[:num_target_classes - 1]

    else:
        raise ValueError("scheme needs to be either 'uniform' or 'data-specific'.")


def Softmax(prediction):
    """
    When ordinal_reg_type is 'separate', the predictions are not probabilities (refer
    to SeparateBinaryOrdReg()). We wish to apply softmax to the last dimension.
    :param prediction: numpy array of shape (n_classes - 1, n_sample, 2)
    :return: predicted probabilities
    """
    input_shape = prediction.shape
    pred_prob = np.exp(prediction) / np.sum(np.exp(prediction), axis=2).reshape(
        input_shape[0], input_shape[1], 1)

    return pred_prob


def OrdRegLoss(prediction, label, ordinal_reg_type, num_target_classes,
               task_weight=None, order_penalty=0, use_gpu=False):
    """
    Customized loss function used to training OrdinalRNNModel. It's important that
    ordinal_reg_type is consistent with how prediction was produced.
    :param prediction: Prediction produced by OrdinalRNNModel.
    :param label: Label formatted by OrdinalLabelReformat and wrapped in pytorch
    tensor.
    :param ordinal_reg_type: Type of ordinal regression.
    Choices are ['joint', 'separate'].
    :param order_penalty: Penalty for unrealistic order. The idea is that in ordinal
    regression, the predicted probability for (label > k) should always be smaller
    than the predicted probability for (label > k - 1).
    :param task_weight_scheme: Refer to TaskWeight(). For now this only applies when
    ordinal_reg_type == 'separate'.
    :return: Loss used to back propagate error gradients.
    """
    if use_gpu and not label.is_cuda:
        # Assume that in this case the content of prediction is already on gpu.
        label = label.cuda()

    if ordinal_reg_type == 'separate':
        loss = 0
        criterion = nn.CrossEntropyLoss()
        if task_weight is None:
            task_weight = np.ones(num_target_classes - 1)
        for i in xrange(num_target_classes - 1):
            # When ordinal_reg_type is "separate", prediction is a pytorch tensor
            # of shape (n_classifier, n_sample, 2). So prediction[i] is the output
            # tensor for the i-th binary classifier. The corresponding response is
            # the i-th column of the formatted label.
            loss += criterion(prediction[i], Variable(label[:, i])) * \
                task_weight[i] / float(num_target_classes - 1)

    if ordinal_reg_type == 'joint':
        # MSE criterion expects float tensor
        if use_gpu:
            label = label.type(torch.cuda.FloatTensor)
        else:
            label = label.type(torch.FloatTensor)
        criterion = nn.MSELoss()
        loss = criterion(prediction, Variable(label))

    if order_penalty > 0:
        # Refer to param description. Penalize unrealistic order prediction.
        relu = torch.nn.ReLU()
        if ordinal_reg_type == 'separate':
            # Need to apply softmax before adding the penalty, because the order can
            # change.
            softmax = torch.nn.Softmax()
            for i in xrange(num_target_classes - 2):
                loss += order_penalty / float(num_target_classes - 2) * torch.mean(
                    relu(softmax(prediction[i + 1])[:, 1] -
                         softmax(prediction[i])[:, 1])
                )

        if ordinal_reg_type == 'joint':
            loss += order_penalty * torch.mean(relu(
                prediction[:, 1:] - prediction[:, :(num_target_classes - 2)]))

    return loss


def Prob2Label(prediction, ordinal_reg_type='separate'):
    """
    Convert predicted probabilities to predicted labels
    :param prediction: np arrays. prediction is of shape (n_classifier, n_sample, 2)
    for 'separate' and (n_sample, n_classifier) for 'joint'.
    :param ordinal_reg_type: Ordinal regression type. Must be consistent with how
    prediction was obtained.
    :return: Vector of predicted labels.
    """
    if ordinal_reg_type == 'separate':
        # Reformat the predicted probabilities to agree with the format of 'joint'.
        prediction = prediction[:, :, 1].reshape(prediction.shape[0],
                                                 prediction.shape[1]).transpose()

    # Now prediction has the format (n_sample, n_classifier) where entry (i, j) is
    # the predicted probability that sample i has label strictly greater than j. We
    # round the probabilities to 0's and 1's and take the row sum to obtain the
    # predicted label.
    prediction = np.sum(np.round(prediction), axis=1).astype(int)

    return prediction


def EqualizeClassSize(feature, label, num_target_classes=None, class_size=None):
    """
    Equalize number of samples in each class by re-sampling, effectively putting more
    weight during training to the samples from smaller classes.
    :param feature: Pandas series of features/predictors.
    :param label: Numpy array of labels/responses. It's important that it's not a
    pandas series which may behave differently to indices and yield NaN.
    :param num_target_classes: Number of target classes. If None, will infer from
    data.
    :param class_size: Number of samples in each class after re-sampling. If None,
    will default to the total number of samples from the original data. Note that if
    it's not big enough, some data points may be lost during re-sampling.
    :return: Re-sampled feature dataset and label dataset.
    """
    if num_target_classes is None:
        num_target_classes = np.max(label) + 1

    if class_size is None:
        class_size = feature.shape[0]

    new_feature_list = []
    new_label_list = []
    for i in xrange(num_target_classes):
        bootstrap_idx = nr.choice(np.where(label == i)[0], size=class_size)
        new_feature_list.append(feature.iloc[bootstrap_idx])
        new_label_list.append(label[bootstrap_idx])

    new_feature = pd.concat(new_feature_list)
    new_label = np.concatenate(tuple(new_label_list))

    return new_feature, new_label


def MacroMeanAbsError(prediction, label, num_target_classes):
    """
    Compute MAE within each class, then average over them. Designed to to robust to
    class imbalance.
    See Evaluation measures for ordinal regression, Baccianella et al., 2009. for
    details.
    """
    mae_per_class = np.zeros(num_target_classes)
    for i in xrange(num_target_classes):
        mae_per_class[i] = np.mean(np.abs(prediction[label==i] - i))

    return np.mean(mae_per_class)


def AllMetrics(prediction, label, num_target_classes=7, binary_cutoff=3):
    """
    Compute all metrics of interest.
    :param prediction: 1-D numpy array of predicted labels
    :param label: 1-D numpy array of true labels
    :param num_target_classes: number of target classes
    :param binary_cutoff: cutoff threshold for binary classification
    :return: (Micro) MAE, Macro MAE, cumulative score 0/acc, cumulative score 1,
    binary acc, weighted binary acc
    """
    mae = MeanAbsError(prediction, label)
    macro_mae = MacroMeanAbsError(prediction, label, num_target_classes)
    cs0 = CumulativeScore(prediction, label, tolerance=0)
    cs1 = CumulativeScore(prediction, label, tolerance=1)
    bin_acc = np.mean(GroupLabels(prediction, binary_cutoff, False) ==
                         GroupLabels(label, binary_cutoff, False))
    weighted_bin_acc = WeightedACC(GroupLabels(prediction, binary_cutoff, False),
                                   GroupLabels(label, binary_cutoff, False))

    return mae, macro_mae, cs0, cs1, bin_acc, weighted_bin_acc