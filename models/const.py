NGRAM_LOW = 1  # Smallest n-gram to be constructed
NGRAM_HIGH = 1  # Largest n-gram to be constructed
LSA = 0  # Whether to apply dimension reduction with Latent Semantic Analysis
N_LSA_COMPONENT = 10000  # Dimension of LSA
N_KMEANS_CLUSTER = 5  # Number of k-means clusters
VERBOSE = 0
SEED = 0

N_SAMPLES = 25000
MAX_FEATURES = 10000
N_TOPICS = 10
N_TOP_WORDS = 20