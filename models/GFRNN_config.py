"""
Configuration for Gated Feedback Recurrent Neural Network.
"""
import argparse

def get_config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--embedding_vector_length', type=int, default=128,
                        help='Length of word embedding')
    parser.add_argument('--hidden_size', type=int, default=128, 
                        help='Length of hidden layer')
    parser.add_argument('--rnn_type', choices=['GFRU', 'LSTM'], default='GFRU',
                        help='RNN type')
    parser.add_argument('--dropout', type=float, default=0.2,
                        help='Drop out')
    parser.add_argument('--rnn_layer_num', type=int, default=3,
                        help='Number of recurrent layers')
    parser.add_argument('--init_lr', type=float, default=3.0,
                        help='Initial learning rate')
    parser.add_argument('--grad_clip', type=float, default=0.25,
                        help='Norm of gradient clipping')
    parser.add_argument('--l2_penalty', type=float, default=0.00001,
                        help='L2 regularization penalty')
    parser.add_argument('--max_eval_batch_size', type=int, default=1500,
                        help='Maximum evaluation batch size. Used to avoid memory'
                             'issues when evaluating big datasets.')
    parser.add_argument('--max_epoch', type=int, default=50,
                        help='Maximum number of epochs')
    parser.add_argument('--decay_factor', type=float, default=2.0,
                        help='Decay factor used during learning rate decay.')
    parser.add_argument('--lr_decay_patience', type=int, default=3,
                        help='Number of epochs to tolerate validation accuracy not'
                             'improving before learning rate decay.')
    parser.add_argument('--early_stop_patience', type=int, default=9,
                        help='Number of epochs to tolerate before early termination')
    parser.add_argument('--batch_size', type=int, default=200,
                        help='Batch size')
    parser.add_argument('--seed', type=int, default=123,
                        help='Seed for reproducibility. Used as initial seed if rep'
                             '> 1')
    parser.add_argument('--rep', type=int, default=1,
                        help='Number of repetitions to run. Starting from initial '
                             'seed, increments seed in each iteration.')
    parser.add_argument('--verbose', type=int, choices=[0, 1], default=0,
                        help='How much info to print')
    parser.add_argument('--batch_normalization', type=int, choices=[0, 1], default=1,
                        help='Whether to apply batch normalization. Recommended.')
    parser.add_argument('--use_gpu', type=int, choices=[0, 1], default=1,
                        help='Whether to use GPU, if available.')
    parser.add_argument('--output_layer_type', choices=['last', 'mean-pool'],
                        default='mean-pool',
                        help='"last" uses the output from the last time point,'
                             '"mean-pool" averages output across time points.')
    parser.add_argument('--residual_connection', type=int, choices=[0, 1], default=1,
                        help='Whether to add residual connection')
    parser.add_argument('--save_model', type=int, choices=[0, 1], default=0,
                        help='Whether to save the model. Note that when rep > 1, '
                             'only the last model will be saved.')
    parser.add_argument('--save_dir_path', default='./trained_models',
                        help='Directory path for saving a trained model')
    parser.add_argument('--load_dir_path', default='./trained_models',
                        help='Directory path for loading a trained model')
    parser.add_argument('--model_name', default='gfrnn',
                        help='Name of the model')
    parser.add_argument('--num_target_classes', type=int, default=7,
                        help='Number of target classes.')
    parser.add_argument('--min_frequency', type=float, default=2,
                        help='Tokens with document appearances strictly lower than '
                             'min_frequency will be dropped')
    parser.add_argument('--input_length', type=int, default=120,
                        help='Length of each text input. Longer text will be '
                             'truncated and shorter text will be 0-padded.')
    parser.add_argument('--join_title_body', type=int, choices=[0, 1], default=1,
                        help='Whether to join title and body')
    parser.add_argument('--embedding_lr_scale', type=float, default=1.0,
                        help='Learning rate for word embedding')
    parser.add_argument('--train_frac', type=float, default=0.7,
                        help='Fraction of data used as the training set')
    parser.add_argument('--valid_frac', type=float, default=0.15,
                        help='Fraction of data used as the validation set')

    # Additional configuration for ordinal regression
    parser.add_argument('--ordinal_reg_type', choices=['separate', 'joint'],
                        default='joint',
                        help='Type of ordinal regression. Refer to '
                             'SeparateBinaryOrdReg() and JointSigmoidOrdReg()')
    parser.add_argument('--task_weight_scheme', choices=['uniform', 'data-specific'],
                        default='data-specific',
                        help='Scheme for task weight.')
    parser.add_argument('--cumulative_score_tolerance', type=int, default=0,
                        help='Tolerance used in computing cumulative score. 0 '
                             'corresponds to computing classification accuracy while'
                             'ignoring the ordinal structure.')
    parser.add_argument('--order_penalty', type=float, default=0.5,
                        help='Penalty for predicted probabilities with unrealistic'
                             'orders')
    parser.add_argument('--equalize_class_size', type=int, choices=[0, 1], default=0,
                        help='Whether to equalize number of samples in each class in'
                             'the training data by re-sampling')

    # Additional config for pretrained embedding
    parser.add_argument('--pretrain_vocab_file_path', default=None,
                        help='File path to the vocabulary used for pretraining '
                             'embedding')
    parser.add_argument('--pretrain_embedding_file_path', default=None,
                        help='File path to the pretrained embedding')

    return parser.parse_args()

config = get_config()