import time
import utility
import mord
import pandas as pd
import numpy as np
import numpy.random as nr
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix


class BaselineClassifier:
    def __init__(self, vectorizer_name='tf-idf', dimension_reduction_name='lsa',
                 classification_name='logistic_regression', seed=0, n_gram_low=1,
                 n_gram_high=1, min_frequency=2, max_features=20000,
                 sublinear_tf=True, n_component=1000, solver='liblinear',
                 multi_class='ovr', svm_kernel='linear', n_tree=500, alpha=1.0):
        self.vectorizer_name = vectorizer_name
        self.dimension_reduction_name = dimension_reduction_name
        self.classification_name = classification_name
        self.tokenizer = lambda doc: doc.lower().split(" ")
        self.vectorizer = None
        self.dimension_reduction_model = None
        self.classification_model = None
        self.seed = seed
        self.trained = False
        nr.seed(seed)

        # If config.min_frequency >= 1, it represents the minimum number of docs a
        # word needs to be in, otherwise it represents the percentage.
        if min_frequency >= 1:
            min_frequency = int(min_frequency)
        if vectorizer_name == 'tf-idf':
            self.vectorizer = TfidfVectorizer(
                analyzer='word', ngram_range=(n_gram_low, n_gram_high),
                max_df=0.95, min_df=min_frequency, norm='l2', use_idf=True,
                smooth_idf=True, sublinear_tf=sublinear_tf,
                tokenizer=self.tokenizer, max_features=max_features)
        elif vectorizer_name == 'tf':
            self.vectorizer = CountVectorizer(
                max_df=0.95, min_df=min_frequency, tokenizer=self.tokenizer,
                max_features=max_features)

        if dimension_reduction_name == 'lsa':
            svd = TruncatedSVD(n_component)
            normalizer = Normalizer(copy=False)
            self.dimension_reduction_model = make_pipeline(svd, normalizer)
        elif dimension_reduction_name == 'nmf':
            self.dimension_reduction_model = NMF(
                n_components=n_component, random_state=seed, alpha=.1, l1_ratio=.5)
        elif dimension_reduction_name == 'lda':
            self.dimension_reduction_model = LatentDirichletAllocation(
                n_components=n_component, max_iter=5, learning_method='online',
                learning_offset=50., random_state=seed)

        if classification_name == 'logistic_regression':
            self.classification_model = LogisticRegression(solver=solver,
                                                           multi_class=multi_class)
        elif classification_name == 'svm':
            self.classification_model = SVC(C=1.0, kernel=svm_kernel)
        elif classification_name == 'random_forest':
            self.classification_model = RandomForestClassifier(n_estimators=n_tree,
                                                               n_jobs=-1)
        elif classification_name == 'ord_logistic_all_threshold':
            self.classification_model = mord.LogisticAT(alpha=alpha)
        elif classification_name == 'ord_logistic_immediate_threshold':
            self.classification_model = mord.LogisticIT(alpha=alpha)
        elif classification_name == 'ord_logistic_squared_error':
            self.classification_model = mord.LogisticSE(alpha=alpha)
        elif classification_name == 'least_abs_deviation':
            self.classification_model = mord.LAD(C=alpha)
        elif classification_name == 'ordinal_ridge':
            self.classification_model = mord.OrdinalRidge(alpha=alpha)

    def fit_model_pipeline(self, features, labels):
        nr.seed(self.seed)

        start_time = time.time()
        features = self.vectorizer.fit_transform(features)
        print "Minutes to fit %s: %3.3f" % (self.vectorizer_name,
                                            (time.time() - start_time) / 60.)

        if str(self.dimension_reduction_name) != 'None':
            start_time = time.time()
            features = self.dimension_reduction_model.fit_transform(features)
            print "Minutes to fit %s: %3.3f:" % (self.dimension_reduction_name,
                                                 (time.time() - start_time) / 60.)

        start_time = time.time()
        self.classification_model.fit(features, labels)
        print "Minutes to fit %s: %3.3f:" % (self.classification_name,
                                             (time.time() - start_time) / 60.)

        self.trained = True

    def predict_pipeline(self, features):
        assert (self.trained == True)

        features = self.vectorizer.transform(features)

        if str(self.dimension_reduction_name) != 'None':
            features = self.dimension_reduction_model.transform(features)

        predictions = self.classification_model.predict(features)

        return predictions


if __name__ == "__main__":
    from baseline_class_config import config

    if config.rep > 1:
        val_acc_all = np.zeros(config.rep)
        val_weighted_acc_all = np.zeros(config.rep)
        test_acc_all = np.zeros(config.rep)
        test_weighted_acc_all = np.zeros(config.rep)

    for iteration in xrange(config.rep):
        # Increment seed every iteration.
        seed = config.seed + iteration

        # Load data
        data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
        data['label'] = utility.GroupLabels(data['label'], cutoff=3, verbose=False)
        if config.join_title_body:
            data['raw'] = data['title'] + ' ' + data['raw']
        train_dat, valid_dat, test_dat = utility.DataSplit(data, config.train_frac,
                                                           config.valid_frac, seed,
                                                           False)

        # Initialize model
        model = BaselineClassifier(
            vectorizer_name=config.vectorizer_name,
            dimension_reduction_name=config.dimension_reduction_name,
            classification_name=config.classification_name, seed=seed,
            n_gram_low=config.n_gram_low, n_gram_high=config.n_gram_high,
            min_frequency=config.min_frequency, max_features=config.max_features,
            sublinear_tf=config.sublinear_tf, n_component=config.n_component,
            solver=config.solver, multi_class=config.multi_class,
            svm_kernel=config.svm_kernel, n_tree=config.n_tree, alpha=config.alpha)

        # Train model
        model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

        # Evaluate model
        valid_pred = model.predict_pipeline(valid_dat['raw'])
        valid_acc = np.mean(valid_pred == valid_dat['label'])
        valid_weighted_acc = utility.WeightedACC(valid_pred, valid_dat['label'])

        test_pred = model.predict_pipeline(test_dat['raw'])
        test_acc = np.mean(test_pred == test_dat['label'])
        test_weighted_acc = utility.WeightedACC(test_pred, test_dat['label'])

        print 'Models used:', config.vectorizer_name, config.dimension_reduction_name, \
            config.classification_name
        print 'Validation acc: %0.3f | Validation weighted acc: %0.3f | Test acc: ' \
              '%0.3f | Test weighted acc: %0.3f' % (valid_acc, valid_weighted_acc,
                                                    test_acc, test_weighted_acc)
        if config.verbose:
            print confusion_matrix(test_dat['label'].values, test_pred)

        if config.rep > 1:
            val_acc_all[iteration] = valid_acc
            val_weighted_acc_all[iteration] = valid_weighted_acc
            test_acc_all[iteration] = test_acc
            test_weighted_acc_all[iteration] = test_weighted_acc

    if config.rep > 1:
        np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
        print '\nSeeds:', range(config.seed, config.seed + config.rep), \
            '\nval_acc_all:', val_acc_all, \
            '\nmean: %5.3f' % val_acc_all.mean(), \
            'stderr: %5.3f' % (val_acc_all.std() / config.rep**0.5), \
            '\nval_weighted_acc_all:', val_weighted_acc_all, \
            '\nmean: %5.3f' % val_weighted_acc_all.mean(), \
            'stderr: %5.3f' % (val_weighted_acc_all.std() / config.rep**0.5), \
            '\ntest_acc_all:', test_acc_all, \
            '\nmean: %5.3f' % test_acc_all.mean(), \
            'stderr: %5.3f' % (test_acc_all.std() / config.rep**0.5), \
            '\ntest_weighted_acc_all:', test_weighted_acc_all, \
            '\nmean: %5.3f' % test_weighted_acc_all.mean(), \
            'stderr: %5.3f' % (test_weighted_acc_all.std() / config.rep**0.5)
