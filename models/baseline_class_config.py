"""
Configuration for baseline_classification. Configuration for each component could be
easily expanded for more customization.
"""
import argparse

def get_config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', type=int, default=0,
                        help='Seed for reproducibility. Used as initial seed if rep'
                             '> 1')
    parser.add_argument('--rep', type=int, default=1,
                        help='Number of repetitions to run. Starting from initial '
                             'seed, increments seed in each iteration.')
    parser.add_argument('--verbose', type=bool, default=False,
                        help='Controls amount of info to print')
    parser.add_argument('--cumulative_score_tolerance', type=int, default=0,
                        help='Tolerance used in computing cumulative score. 0 '
                             'corresponds to computing classification accuracy while'
                             'ignoring the ordinal structure.')
    # Configuration related to data prep
    parser.add_argument('--train_frac', type=float, default=0.7,
                        help='Fraction of data used as the training set')
    parser.add_argument('--valid_frac', type=float, default=0.15,
                        help='Fraction of data used as the validation set')
    parser.add_argument('--join_title_body', type=bool, default=True,
                        help='Whether to join title and body')

    # Configuration related to vectorizer
    parser.add_argument('--vectorizer_name', choices=['tf-idf', 'tf'], default='tf-idf',
                        help='Name of vectorizer to be used.')
    parser.add_argument('--n_gram_low', type=int, default=1,
                        help='Smallest n-gram to be constructed')
    parser.add_argument('--n_gram_high', type=int, default=1,
                        help='Largest n-gram to be constructed')
    parser.add_argument('--min_frequency', type=float, default=2,
                        help='Tokens with document appearances strictly lower than '
                             'min_frequency will be dropped')
    parser.add_argument('--max_features', type=int, default=20000,
                        help='Maximum number of features created by vectorizer')
    parser.add_argument('--sublinear_tf', type=bool, default=True,
                        help='Replace tf with 1 + log(tf)')

    # Configuration related to dimension reduction model
    parser.add_argument('--dimension_reduction_name',
                        choices=['None', 'lsa', 'nmf', 'lda'], default='None',
                        help='Name of dimension reduction model. Choose among Latent'
                             'Semantic Analysis, Non-negative Matrix Factorization,'
                             'and Latent Dirichlet Allocation.')
    parser.add_argument('--n_component', type=int, default=1000,
                        help='Number of components in the dimension reduction')

    # Configuration related to classification model
    parser.add_argument('--classification_name',
                        choices=['logistic_regression', 'svm', 'random_forest',
                                 'ord_logistic_all_threshold',
                                 'ord_logistic_immediate_threshold',
                                 'ord_logistic_squared_error',
                                 'least_abs_deviation', 'ordinal_ridge'],
                        default='svm',
                        help='Name of classification model to be used')
    parser.add_argument('--solver', choices=['liblinear', 'lbfgs'],
                        default='liblinear',
                        help='Optimization algorithm to use when fitting logistic'
                             'regression. Note that liblinear only supports ovr.')
    parser.add_argument('--multi_class', choices=['ovr', 'multinomial'],
                        default='ovr',
                        help='Scheme for multi-class classification in logistic'
                             'regression. One vs rest or multinomial.')
    parser.add_argument('--svm_kernel', choices=['linear', 'poly', 'rbf', 'sigmoid'],
                        default='linear',
                        help='Kernel type for SVM')
    parser.add_argument('--n_tree', type=int, default=500,
                        help='Number of trees in random forest')
    parser.add_argument('--alpha', type=float, default=1.0,
                        help='l2 regularization parameter')

    return parser.parse_args()

config = get_config()
