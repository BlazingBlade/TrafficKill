import emoji
import string
import re
import time
import copy
import const
import numpy as np
import numpy.random as nr
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.cluster import KMeans
from sklearn import metrics
from sklearn.externals import joblib

nr.seed(const.SEED)

# Read data prepared by tna_data_prep. Will do tna_review and tna_thread after
# finishing tna_ad.
data = pd.read_pickle('tna_ad.pkl')

# The default tokenizer from sklearn deletes emojis, so we define our own
# tokenizer to override it.
tokenizer = lambda doc: doc.lower().split(" ")

# Create tf-idf vectorizer, which performs both ngram vectorization and
# tf-idf weighting. Note that when max_df and min_df are integers, they
# represent count. When they are floats, they represent percentages. min_df=1
# allows us to drop ngrams that appeared in only one post, such as phone numbers,
# which are no longer useful for text analysis after being extracted separately.
# For complete documentation of TfidfVectorizer see
# http://scikit-learn.org/stable/modules/generated/
# sklearn.feature_extraction.text.TfidfVectorizer.html
tf_idf_vectorizer = TfidfVectorizer(
    analyzer='word', ngram_range=(const.NGRAM_LOW, const.NGRAM_HIGH), max_df=1.0,
    min_df=1, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False,
    tokenizer=tokenizer)
# Convert text to a matrix of vectors, where each row is the ngram count vector
# re-weighted by tf-idf.
start_time = time.time()
mat = tf_idf_vectorizer.fit_transform(data['raw'])
print "Minutes to vectorize texts:", (time.time() - start_time) / 60

# Dimension reduction with Latent Semantic Analysis (optional, memory intensive)
if const.LSA:
    start_time = time.time()
    svd = TruncatedSVD(const.N_LSA_COMPONENT)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    mat = lsa.fit_transform(mat)
    print "Minutes to perform LSA:", (time.time() - start_time) / 60

    explained_var = svd.explained_variance_ratio_.sum()
    print "Explained variance of the SVD step:", explained_var

# K-means clustering.
start_time = time.time()
kmeans_model = KMeans(n_clusters=const.N_KMEANS_CLUSTER, init='k-means++',
                      max_iter=300, n_init=5, n_jobs=-1, verbose=const.VERBOSE)
kmeans_model.fit(mat)
print "Minutes to perform KMeans:", (time.time() - start_time) / 60
# Save model
joblib.dump(kmeans_model, 'kmeans_model.pkl')


# A quick look at the 5 clusters didn't reveal any "trafficker cluster". And I
# suppose there's no reason one should've been identified, because there're simply
# too many ways to cluster the texts.
pd.set_option('display.max_colwidth', -1)
for i in xrange(const.N_KMEANS_CLUSTER):
    print data['raw'][kmeans_model.labels_==i][:30]


# # Some useful text manipulation for future references:
# # Example of tokenizing a string separately:
# analyzer = tf_idf_vectorizer.build_analyzer()
# analyzer(data['raw'][0])
#
# # Example of getting the column index of a token:
# tf_idf_vectorizer.vocabulary_.get(u'\ufe0f')
#
# # Example of transforming a string:
# tf_idf_vectorizer.transform(data['raw'][0])