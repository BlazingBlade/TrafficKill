import utility
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
from baseline_classification import BaselineClassifier
from baseline_class_config import config

if __name__ == '__main__':

    if config.rep > 1:
        val_mae_all = np.zeros(config.rep)
        val_cs_all = np.zeros(config.rep)
        test_mae_all = np.zeros(config.rep)
        test_cs_all = np.zeros(config.rep)
        binary_test_acc = np.zeros(config.rep)
        binary_test_weighted_acc = np.zeros(config.rep)

    for iteration in xrange(config.rep):
        # Increment seed every iteration.
        seed = config.seed + iteration

        # Load data
        data = pd.read_pickle('data/trafficking10k_cleaned.pkl')
        if config.join_title_body:
            data['raw'] = data['title'] + ' ' + data['raw']
        train_dat, valid_dat, test_dat = utility.DataSplit(data, config.train_frac,
                                                           config.valid_frac, seed,
                                                           False)

        # Initialize model
        model = BaselineClassifier(
            vectorizer_name=config.vectorizer_name,
            dimension_reduction_name=config.dimension_reduction_name,
            classification_name=config.classification_name, seed=seed,
            n_gram_low=config.n_gram_low, n_gram_high=config.n_gram_high,
            min_frequency=config.min_frequency, max_features=config.max_features,
            sublinear_tf=config.sublinear_tf, n_component=config.n_component,
            solver=config.solver, multi_class=config.multi_class,
            svm_kernel=config.svm_kernel, n_tree=config.n_tree)

        # Train model
        model.fit_model_pipeline(train_dat['raw'], train_dat['label'])

        # Evaluate model
        val_pred = model.predict_pipeline(valid_dat['raw'])
        val_mae = utility.MeanAbsError(val_pred, valid_dat['label'].values)
        val_cs = utility.CumulativeScore(val_pred, valid_dat['label'].values,
                                         tolerance=config.cumulative_score_tolerance)

        test_pred = model.predict_pipeline(test_dat['raw'])
        test_mae = utility.MeanAbsError(test_pred, test_dat['label'].values)
        test_cs = utility.CumulativeScore(test_pred, test_dat['label'].values,
                                          tolerance=config.cumulative_score_tolerance)

        print 'Models used:', config.vectorizer_name, config.dimension_reduction_name, \
            config.classification_name
        print 'Validation mean abs error: %5.3f | Validation cumulative score: %0.3f ' \
              '| Test mean abs error: %5.3f | Test cumulative score: %0.3f' % (
            val_mae, val_cs, test_mae, test_cs)

        # Check out the confusion matrix
        if config.verbose:
            print confusion_matrix(test_dat['label'].values, test_pred)

        # Check out the binary classification performance of the ordinal regression model
        pred_label = utility.GroupLabels(test_pred, cutoff=3, verbose=False)
        truth = utility.GroupLabels(test_dat['label'], cutoff=3, verbose=False)
        test_acc = np.mean(pred_label == truth)
        test_weighted_acc = utility.WeightedACC(pred_label, truth)
        print 'Binary classification test_acc: %5.3f | test_weighted_acc: %5.3f' % (
            test_acc, test_weighted_acc
        )

        if config.rep > 1:
            val_mae_all[iteration] = val_mae
            val_cs_all[iteration] = val_cs
            test_mae_all[iteration] = test_mae
            test_cs_all[iteration] = test_cs
            binary_test_acc[iteration] = test_acc
            binary_test_weighted_acc[iteration] = test_weighted_acc

    if config.rep > 1:
        np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
        print '\nSeeds:', range(config.seed, config.seed + config.rep), \
            '\nval_mae_all:', val_mae_all, \
            '\nmean: %5.3f' % val_mae_all.mean(), \
            'stderr: %5.3f' % (val_mae_all.std() / config.rep**0.5), \
            '\nval_cs_all:', val_cs_all, \
            '\nmean: %5.3f' % val_cs_all.mean(), \
            'stderr: %5.3f' % (val_cs_all.std() / config.rep**0.5), \
            '\ntest_mae_all:', test_mae_all, \
            '\nmean: %5.3f' % test_mae_all.mean(), \
            'stderr: %5.3f' % (test_mae_all.std() / config.rep**0.5), \
            '\ntest_cs_all:', test_cs_all, \
            '\nmean: %5.3f' % test_cs_all.mean(), \
            'stderr: %5.3f' % (test_cs_all.std() / config.rep**0.5), \
            '\nbinary_test_acc:', binary_test_acc, \
            '\nmean: %5.3f' % binary_test_acc.mean(), \
            'stderr: %5.3f' % (binary_test_acc.std() / config.rep**0.5), \
            '\nbinary_test_weighted_acc:', binary_test_weighted_acc, \
            '\nmean: %5.3f' % binary_test_weighted_acc.mean(), \
            'stderr: %5.3f' % (binary_test_weighted_acc.std() / config.rep**0.5)